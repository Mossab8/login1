@Logout
Feature: Logout from the website page

  Scenario: verify the logging out form the homepage
    Given you are inside the website page
    When navigate to logout button
    Then i should be logged out and see the login page