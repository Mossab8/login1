package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class openWebpage {
    public static WebDriver driver;


    @Given("navigate to the website page")
    public void navigate_to_the_website_page() {

        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://automationexercise.com");
    }
    @When("navigate to signup link")
    public void navigate_to_signup_link() {
        driver.findElement(By.xpath("//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[4]/a")).click();

    }
    @Then("i should see the signup page opened")
    public void i_should_see_the_signup_page_opened() {

            Assert.assertEquals(driver.getTitle(),"Automation Exercise - Signup / Login");

    }

}
