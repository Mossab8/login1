package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;



public class logout {
    public static WebDriver driver;



    @Given("you are inside the website page")
    public void you_are_inside_the_website_page() {

        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://automationexercise.com");
    }

    @When("navigate to logout button")
    public void navigate_to_logout_button() {


       WebElement Logout = driver.findElement(By.xpath("//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[4]/a"));
        Logout.click();
    }

    @Then("i should be logged out and see the login page")
    public void i_should_be_logged_out_and_see_the_login_page() {


        Assert.assertEquals(driver.getTitle(),"Automation Exercise - Signup / Login");
        driver.quit();

    }

}
